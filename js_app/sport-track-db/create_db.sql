DROP TABLE IF EXISTS Data;
DROP TABLE IF EXISTS Activity;
DROP TABLE IF EXISTS User;
--The User table is the table which will contain informations about an account--
CREATE TABLE User(

    lastName TEXT 
        CONSTRAINT nn_lastNameUser NOT NULL,

    firstName TEXT 
        CONSTRAINT nn_fistNameUser NOT NULL,
    
    birthday DATE 
        CONSTRAINT nn_birthdayUser NOT NULL,

    gender TEXT 
        CONSTRAINT nn_genderUser NOT NULL 
        CONSTRAINT ck_syntaxeGender CHECK(gender='MAN' OR gender='WOMAN'),

    height INTEGER 
        CONSTRAINT nn_heighthUser NOT NULL
        CONSTRAINT ck_syntaxeWeight CHECK(height>20 AND height<250),

    weight INTEGER 
        CONSTRAINT nn_weightUser NOT NULL
        CONSTRAINT ck_syntaxeWeight CHECK(weight>20 AND weight<250),

    email TEXT 
        CONSTRAINT pk_User PRIMARY KEY 
        CONSTRAINT ck_syntaxeEmail CHECK (email LIKE '%_@_%._%'),

    password TEXT 
        CONSTRAINT nn_passwordUser NOT NULL
        CONSTRAINT ck_passwordUser CHECK (LENGTH(RTRIM(password)) >= 8)   
    
);
--The Activity table is the table which will contain informations about an activity--
CREATE TABLE Activity(

    id_activity INTEGER 
        CONSTRAINT pk_Activity PRIMARY KEY,
    
    date TEXT 
        CONSTRAINT nn_date NOT NULL,

    description TEXT
        CONSTRAINT nn_description NOT NULL,

    timeTotal INTEGER 
        CONSTRAINT nn_timeTotal NOT NULL,

    averageCardioFrequency INTEGER 
        CONSTRAINT nn_averageCardioFrequency NOT NULL,
    
    distance INTEGER
        CONSTRAINT nn_distance NOT NULL
        CONSTRAINT ck_distance CHECK(distance>0),

    startTime DATETIME
        CONSTRAINT nn_startTime NOT NULL,

    minCardio INTEGER 
        CONSTRAINT nn_minCardio NOT NULL
        CONSTRAINT ck_minCardio CHECK (minCardio>0 AND minCardio<220),


    maxCardio INTEGER 
        CONSTRAINT nn_maxCardio NOT NULL
        CONSTRAINT ck_maxCardio CHECK (maxCardio>0 AND maxCardio<220),

    theUser TEXT
        CONSTRAINT fk_Activity_User REFERENCES User(email)
);

--The Data table is the table which will contains many data for one activity--
CREATE TABLE Data(

    id_data INTEGER
        CONSTRAINT pk_Data PRIMARY KEY,

    time INTEGER 
        CONSTRAINT nn_time NOT NULL,
    
    cardioFrequency INTEGER 
        CONSTRAINT nn_cardioFrequence NOT NULL
        CONSTRAINT ck_cardioFrequency CHECK (cardioFrequency>0 AND cardioFrequency<220),
    
    latitude INTEGER 
        CONSTRAINT nn_latitude NOT NULL 
        CONSTRAINT ck_latitude CHECK (latitude>-90 AND latitude<90),

    longitude INTEGER 
        CONSTRAINT nn_longitude NOT NULL 
        CONSTRAINT ck_longitude CHECK (longitude>-180 AND longitude<180),

    altitude INTEGER 
        CONSTRAINT nn_altitude NOT NULL,

    theActivity INTEGER
        CONSTRAINT fk_Data_Activity REFERENCES Activity(id_activity)
);

DROP TRIGGER IF EXISTS trig_freq;


CREATE TRIGGER trig_card
BEFORE INSERT
ON Activity
BEGIN
    SELECT
    CASE
    WHEN (NEW.minCardio > NEW.averageCardioFrequency) OR (NEW.maxCardio < NEW.averageCardioFrequency) THEN
    RAISE (ABORT,'the average heart rate cannot be lower than minimum cardio or higher than maximum cardio')
    END;
END;
