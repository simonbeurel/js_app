var db = require('./sqlite_connection');

var ActivityDAO = function (){

    this.insert = function(values, callback){

        db.run(`INSERT INTO Activity ("id_activity", "date", "description", "timeTotal", "averageCardioFrequency", "distance", "startTime", "minCardio", "maxCardio", "theUser") VALUES (?,?,?,?,?,?,?,?, ?, ?)`, values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9], function(err) {
            if (err) {
                return console.log(err.message);
            }
            callback();

        });

    };

    this.delete = function(key, callback){

        db.run(`DELETE FROM Activity WHERE id_activity=?`, key, function(err) {
            if (err) {
                return console.log(err.message);
            }
            callback();
        });

    };

    this.update = function(key, values, callback){

        this.delete(key,callback);
        this.insert(values, callback);

        callback();

    };


    this.findAll = function(callback){

        db.all("SELECT * FROM Activity", [], (err, rows) => {
            if (err) {
                throw err;
            }
            callback(rows);
        })
    };

    this.findByKey = function(key, callback){

        db.get("SELECT * FROM Activity where id_activity=?", key, (err, row) => {
            if (err) {
                return console.error(err.message);
            }
            callback(row);

        });

    };

    this.deleteAll = function(callback){

        db.run(`DELETE * FROM Activity`, (error) => {
            if (error) {
                console.error(error);
            }

            callback();
        });

    };

};

var dao = new ActivityDAO();
module.exports = dao;